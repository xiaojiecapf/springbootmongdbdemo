package org.huxd.springbootmongdb.demo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName: User
 * @Description: TOFO
 * @author: huxudong
 * @Date: 2020/4/4 13:09
 * @Version 1.0
 */
@Data
@Document("user")
public class User  {
    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("password")
    private String password;

    @Field("address")
    private String address;

    @Field("create_time")
    private Date createTime;

    @Field("last_update_time")
    private Date lastUpdateTime;


}
